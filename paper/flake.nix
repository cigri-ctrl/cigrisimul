{
  description = "Markdown template";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
  inputs.qornflakes.url = "github:GuilloteauQ/qornflakes";

  outputs = { self, nixpkgs, qornflakes }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      tag = "sigri";

      flakeImage = pkgs.dockerTools.pullImage {
        imageName = "nixpkgs/nix-flakes";
        imageDigest =
          "sha256:86f00aa1c5b35c147f689ce9d0762d07bc477387726afad5f958238bab0b90d8";
        sha256 = "sha256-rnP1Lmf6G6K1Yazwy6r04zWmElcCvEVpW8kpy4pXvrM";
        os = "linux";
        arch = "amd64";
      };

      rBaseInputs = with pkgs;
        [
          rPackages.tidyverse
          rPackages.zoo
          rPackages.reshape2
          rPackages.broom
          rPackages.gtsummary
          rPackages.gt
          rPackages.kableExtra
          rPackages.stargazer
          rPackages.pander
          rPackages.goftest
          # rPackages.equatiomatic
          rPackages.modelr
          rPackages.fitdistrplus
          qornflakes.packages.x86_64-linux.facetscales
        ];

      rmdInputs = with pkgs; [
        rPackages.rmarkdown
        rPackages.markdown
        rPackages.knitr
        rPackages.magick
        rPackages.codetools
        rPackages.bibtex
      ];

      myR = (pkgs.rWrapper.override { packages = rBaseInputs ++ rmdInputs; });

      buildInputs = [
        myR
        pkgs.coreutils
        pkgs.pandoc
        pkgs.texlive.combined.scheme-full
        pkgs.which
      ];

    in {
      devShell.x86_64-linux =
        pkgs.mkShell { buildInputs = buildInputs ++ [ pkgs.rstudio ]; };

      defaultPackage.x86_64-linux = self.packages.x86_64-linux.pdf;

      packages.x86_64-linux = {
        pdf = with pkgs;
          stdenv.mkDerivation {
            name = "pdf";
            inherit buildInputs;
            src = ./.;
            installPhase = ''
              mkdir -p $out
              echo 'rmarkdown::render("./paper/main.Rmd", "pdf_document")' > .build.R
              ${myR}/bin/Rscript .build.R
              mv ./paper/main.pdf $out/
            '';
          };
        html = with pkgs;
          stdenv.mkDerivation {
            name = "html";
            inherit buildInputs;
            src = ./.;
            installPhase = ''
              mkdir -p $out
              echo 'rmarkdown::render("./paper/main.Rmd", "html_document")' > .build.R
              ${myR}/bin/Rscript .build.R
              mv ./paper/main.html $out/
            '';
          };

        imageCI = with pkgs;
          dockerTools.buildImageWithNixDb {
            name = "guilloteauq/nix-rmd";
            inherit tag;
            fromImage = flakeImage;
            contents = buildInputs;
          };
      };
    };
}
