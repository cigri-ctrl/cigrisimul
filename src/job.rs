use serde::Deserialize;
use std::collections::HashMap;
pub type JobId = usize;

#[derive(Deserialize, Debug, Clone, Eq, PartialEq, Copy, Hash)]
pub enum IOWeight {
    IOHeavy,
    IOLight,
}

impl Default for IOWeight {
    fn default() -> Self {
        IOWeight::IOHeavy
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum JobStatus {
    Created,
    OARSubmitted,
    OARWaiting,
    FileSystemDone,
    FileSystemWaiting,
    FileSystemExecuting,
    Executing,
    Done,
}

#[derive(Debug, Clone, Eq, PartialEq, Copy, Hash)]
pub enum Priority {
    BestEffort,
    Normal,
}

#[derive(Debug, Clone)]
pub struct Job {
    job_id: usize,
    status: JobStatus,
    duration: f64,
    file_size: f64,
    priority: Priority,
    start_time: Option<f64>,
}

impl Job {
    pub fn new(duration: f64, file_size: f64, job_id: usize, priority: Priority) -> Self {
        Job {
            job_id,
            status: JobStatus::Created,
            duration,
            file_size,
            priority,
            start_time: None,
        }
    }

    pub fn get_priority(&self) -> Priority {
        self.priority
    }

    pub fn get_duration(&self) -> f64 {
        self.duration
    }

    pub fn get_file_size(&self) -> f64 {
        self.file_size
    }

    pub fn get_status(&self) -> JobStatus {
        self.status
    }

    pub fn set_status(&mut self, status: JobStatus) {
        self.status = status;
    }

    pub fn get_start_time(&self) -> Option<f64> {
        self.start_time
    }

    pub fn set_start_time(&mut self, start_time: Option<f64>) {
        self.start_time = start_time
    }
}

#[derive(Debug, Clone)]
pub struct Campaign {
    campaign_id: usize,
    pub nb_jobs: usize,
    pub jobs: Vec<usize>,
    priority: Priority,
    pub io_weight: IOWeight,
}

impl Campaign {
    pub fn new(
        campaign_id: usize,
        nb_jobs: usize,
        jobs: Vec<usize>,
        priority: Priority,
        io_weight: IOWeight,
    ) -> Self {
        Campaign {
            campaign_id,
            nb_jobs,
            jobs,
            priority,
            io_weight,
        }
    }

    pub fn get_id(&self) -> usize {
        self.campaign_id
    }

    pub fn get_io_weight(&self) -> IOWeight {
        self.io_weight
    }
}

#[derive(Clone, Default)]
pub struct JobBank {
    current_job_id: JobId,
    current_campaign_id: usize,
    bank: HashMap<JobId, Job>,
}

impl JobBank {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_current_job_id(&self) -> JobId {
        self.current_job_id
    }

    pub fn increase_job_id(&mut self) {
        self.current_job_id = self.current_job_id + 1;
    }

    pub fn get_current_campaign_id(&self) -> usize {
        self.current_campaign_id
    }

    pub fn increase_campaign_id(&mut self) {
        self.current_campaign_id = self.current_campaign_id + 1;
    }

    pub fn create_new_job(&mut self, duration: f64, io_duration: f64, priority: Priority) -> JobId {
        let job_id = self.get_current_job_id();
        let job = Job::new(duration, io_duration, job_id, priority);
        self.bank.insert(job_id, job);
        self.increase_job_id();
        job_id
    }

    pub fn get_priority(&self, job_id: JobId) -> Priority {
        self.bank.get(&job_id).unwrap().get_priority()
    }

    pub fn get_duration(&self, job_id: JobId) -> f64 {
        self.bank.get(&job_id).unwrap().get_duration()
    }

    pub fn get_file_size(&self, job_id: JobId) -> f64 {
        self.bank.get(&job_id).unwrap().get_file_size()
    }

    pub fn create_new_campaign(
        &mut self,
        nb_jobs: usize,
        duration: f64,
        io_duration: f64,
        io_weight: IOWeight,
    ) -> Campaign {
        let priority = Priority::BestEffort;
        let campaign_id = self.get_current_campaign_id();
        let jobs = (0..nb_jobs)
            .map(|_| self.create_new_job(duration, io_duration, priority))
            .collect::<Vec<JobId>>();
        self.increase_campaign_id();
        Campaign::new(campaign_id, nb_jobs, jobs, priority, io_weight)
    }

    pub fn has_been_executed(&self, job_id: JobId) -> bool {
        self.bank.get(&job_id).unwrap().get_status() == JobStatus::Done
    }

    pub fn get_status(&self, job_id: JobId) -> JobStatus {
        self.bank.get(&job_id).unwrap().get_status()
    }

    pub fn set_status(&mut self, job_id: JobId, status: JobStatus) {
        self.bank.get_mut(&job_id).unwrap().set_status(status);
    }

    pub fn get_start_time(&self, job_id: JobId) -> Option<f64> {
        self.bank.get(&job_id).unwrap().get_start_time()
    }

    pub fn set_start_time(&mut self, job_id: JobId, start_time: Option<f64>) {
        self.bank.get_mut(&job_id).unwrap().set_start_time(start_time);
    }
}
