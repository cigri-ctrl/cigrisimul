#[derive(Default, Debug, Clone)]
pub struct Clock {
    current_time: f64,
}

impl Clock {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_time(&self) -> f64 {
        self.current_time
    }

    pub fn set_time(&mut self, time: f64) {
        self.current_time = time;
    }
}
