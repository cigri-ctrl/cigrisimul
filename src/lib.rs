mod cigri;
mod clock;
mod controllers;
mod event;
mod file_system;
mod job;
mod local_users;
mod oar;
use cigri::CiGri;
use clock::Clock;
use controllers::*;
use event::{Action, Event, EventList, EventManager};
use file_system::FileSystem;
use job::*;
use local_users::LocalUsers;
use oar::OAR;
use serde::Deserialize;
use std::cell::RefCell;
use std::env;
use std::fs::File;
use std::io::{Write,Read};

use wasm_bindgen::prelude::*;

#[derive(Deserialize, Clone)]
struct CampaignDescription {
    nb_jobs: usize,
    job_duration: f64,
    file_size: f64,
    #[serde(default)]
    io_weight: IOWeight,
}

#[derive(Deserialize)]
struct SimulPlan {
    oar_resources: usize,
    file_system_nb_threads: usize,
    cigri_sampling_time: f64,
    controller: Box<ControllerType>,
    campaigns: Vec<CampaignDescription>,
    #[serde(default)]
    local_users: Vec<LocalUsers>,
    #[serde(default)]
    noise: bool,
    #[serde(default)]
    horizon_start: Option<f64>,
    #[serde(default)]
    horizon_end: Option<f64>,
}

impl SimulPlan {
    fn start(&mut self) -> String {
        let clock = Clock::new();

        let event_list = EventList::new();
        let rc_event_list = RefCell::new(event_list);

        let rc_clock = RefCell::new(clock);

        let job_db = JobBank::new();
        let rc_job_db = RefCell::new(job_db);
        let fs = FileSystem::new(
            self.file_system_nb_threads,
            &rc_clock,
            &rc_event_list,
            &rc_job_db,
            self.noise,
        );
        let rc_fs = RefCell::new(fs);
        let oar = OAR::new(
            self.oar_resources,
            &rc_clock,
            &rc_event_list,
            &rc_job_db,
            &rc_fs,
        );
        let rc_oar = RefCell::new(oar);

        let controller = RefCell::new(&mut self.controller);
        let cigri = CiGri::new(
            self.cigri_sampling_time,
            &rc_clock,
            &rc_event_list,
            &rc_job_db,
            &rc_oar,
            &controller,
        );

        let rc_cigri = RefCell::new(cigri);

        for c in &self.campaigns {
            let campaign = rc_job_db.borrow_mut().create_new_campaign(
                c.nb_jobs,
                c.job_duration,
                c.file_size,
                c.io_weight,
            );
            rc_cigri.borrow_mut().submit(campaign);
        }

        for lu in &self.local_users {
            lu.generate(&rc_job_db, &rc_event_list, self.horizon_start, self.horizon_end);
        }

        let mut event_manager =
            EventManager::new(&rc_clock, &rc_event_list, &rc_oar, &rc_cigri, &rc_fs);
        rc_event_list
            .borrow_mut()
            .push(Event::new(0.0, Action::CiGriMainLoop));

        rc_event_list
            .borrow_mut()
            .push(Event::new(0.0, Action::FileSystemMeasure));

        event_manager.run();
        let log = rc_cigri.borrow().get_log();
        log
    }
}

#[wasm_bindgen]
pub fn add_stuff(filename: &str) -> String {
    format!("filename: {}", filename)
}

#[wasm_bindgen]
pub fn get_config(filename: &str) -> String {
    std::fs::read_to_string(filename).unwrap_or(String::from("oops"))//expect("Something went wrong reading the config file")
}

#[wasm_bindgen]
pub fn js_start(config: &str) -> String {
    let mut simulation_plan: SimulPlan =
        serde_yaml::from_str(&config).expect("Could not read config file");

    simulation_plan.start()
}

#[wasm_bindgen]
pub fn start_simul_from_file(filename: &str, outfilename: &str) {
    let config =
        std::fs::read_to_string(filename).expect("Something went wrong reading the config file");

    let mut simulation_plan: SimulPlan =
        serde_yaml::from_str(&config).expect("Could not read config file");
    let mut file = File::create(outfilename).expect("oops");
    let result = simulation_plan.start();
    write!(file, "{}", result).expect("could not write to file");
}
