use crate::clock::Clock;
use crate::controllers::*;
use crate::event::{Action, Event, EventList};
use crate::file_system::FileSystem;
use crate::job::*;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet, VecDeque};

#[derive(Clone)]
pub struct OAR<'a> {
    max_resources: usize,
    running_jobs: HashSet<JobId>,
    waiting_jobs: HashMap<Priority, VecDeque<JobId>>,
    event_list: &'a RefCell<EventList>,
    global_clock: &'a RefCell<Clock>,
    job_db: &'a RefCell<JobBank>,
    fs: &'a RefCell<FileSystem<'a>>,
    notify_jobs: isize,
    total_be_jobs_killed: f64,
}

impl<'a> OAR<'a> {
    pub fn new(
        max_resources: usize,
        global_clock: &'a RefCell<Clock>,
        event_list: &'a RefCell<EventList>,
        job_db: &'a RefCell<JobBank>,
        fs: &'a RefCell<FileSystem<'a>>,
    ) -> Self {
        OAR {
            max_resources,
            running_jobs: HashSet::new(),
            waiting_jobs: HashMap::new(),
            event_list: event_list,
            global_clock: global_clock,
            job_db,
            fs,
            notify_jobs: 0,
            total_be_jobs_killed: 0.0,
        }
    }

    pub fn notify_start(&mut self) {
        self.notify_jobs += 1
    }

    pub fn notify_end(&mut self) {
        self.notify_jobs -= 1
    }

    pub fn get_nb_free_resources(&self) -> usize {
        self.max_resources - self.running_jobs.len()
    }

    pub fn get_nb_waiting_jobs(&self, priority: Priority) -> Option<usize> {
        self.waiting_jobs.get(&priority).map(|x| x.len())
    }

    pub fn submit_job(&mut self, job_id: JobId) {
        let job_priority = self.job_db.borrow().get_priority(job_id);
        self.waiting_jobs
            .entry(job_priority)
            .or_insert(VecDeque::new())
            .push_back(job_id);
        let new_event = Event::new(0.0, Action::OARSchedule);
        self.job_db
            .borrow_mut()
            .set_status(job_id, JobStatus::OARWaiting);
        self.event_list.borrow_mut().push(new_event);
    }

    fn kill_besteffort_jobs_running(&mut self, nb_to_kill: usize) {
        let to_kill: Vec<JobId> = self
            .running_jobs
            .iter()
            .filter(|&j_id| self.job_db.borrow().get_priority(*j_id) == Priority::BestEffort)
            .map(|&x| x)
            .collect();
        let current_time = self.global_clock.borrow().get_time();
        to_kill.iter().take(nb_to_kill).for_each(|j_id| {
            let start_time = self.job_db.borrow_mut().get_start_time(*j_id).unwrap();
            self.job_db.borrow_mut().set_start_time(*j_id, None);
            self.total_be_jobs_killed += current_time - start_time;
            let _ = self.running_jobs.remove(j_id);
            self.fs.borrow_mut().kill_job(*j_id);
            self.submit_job(*j_id);
        });
    }

    fn schedule_waiting_jobs_with_priority(&mut self, priority: Priority, nb_max: usize) -> usize {
        let mut i = 0;
        let current_time = self.global_clock.borrow().get_time();
        let opt_initial_nb_of_waiting_jobs: Option<usize> = self.get_nb_waiting_jobs(priority);
        if let Some(initial_nb_of_waiting_jobs) = opt_initial_nb_of_waiting_jobs {
            while i < initial_nb_of_waiting_jobs && i < nb_max {
                let job_id = self
                    .waiting_jobs
                    .get_mut(&priority)
                    .unwrap()
                    .pop_front()
                    .unwrap();
                assert!(
                    self.job_db.borrow().get_status(job_id) == JobStatus::OARWaiting,
                    "job status found {:?}",
                    self.job_db.borrow().get_status(job_id)
                );
                self.job_db
                    .borrow_mut()
                    .set_status(job_id, JobStatus::Executing);
                self.job_db.borrow_mut().set_start_time(job_id, Some(current_time));
                let new_event = Event::new(
                    self.global_clock.borrow().get_time()
                        + self.job_db.borrow().get_duration(job_id),
                    Action::FileSystemSubmit(job_id),
                );

                self.running_jobs.insert(job_id);
                self.event_list.borrow_mut().push(new_event);
                i = i + 1;
            }
        }

        nb_max - i
    }

    pub fn schedule_jobs(&mut self) {
        let nb_waiting_jobs_normal = self.get_nb_waiting_jobs(Priority::Normal).unwrap_or(0);
        let nb_free_resources = self.get_nb_free_resources();
        if nb_waiting_jobs_normal > nb_free_resources {
            self.kill_besteffort_jobs_running(nb_waiting_jobs_normal - nb_free_resources);
        }
        let new_nb_free_resources = self.get_nb_free_resources();
        let remaining_free_resources =
            self.schedule_waiting_jobs_with_priority(Priority::Normal, new_nb_free_resources);
        let _ = self
            .schedule_waiting_jobs_with_priority(Priority::BestEffort, remaining_free_resources);
    }

    pub fn terminate_job(&mut self, job_id: usize) {
        assert!(
            self.job_db.borrow().get_status(job_id) == JobStatus::FileSystemDone,
            "job status found {:?}",
            self.job_db.borrow().get_status(job_id)
        );

        self.job_db.borrow_mut().set_status(job_id, JobStatus::Done);
        self.running_jobs.remove(&job_id);
        let new_event = Event::new(0.0, Action::OARSchedule);
        self.event_list.borrow_mut().push(new_event);
    }

    pub fn get_nb_running_be_jobs(&self) -> usize {
        self.running_jobs.iter().filter(|&job_id| self.job_db.borrow_mut().get_priority(*job_id) == Priority::BestEffort).count()
    }

    pub fn get_sensor_values(&self) -> SensorValues {
        SensorValues::new(
            self.waiting_jobs.iter().map(|(_, v)| v.len()).sum(),
            self.running_jobs.len(),
            self.fs.borrow().get_loadavg(),
            self.get_nb_running_be_jobs(),
            self.get_nb_waiting_jobs(Priority::BestEffort).unwrap_or(0),
            self.notify_jobs,
            self.total_be_jobs_killed,
        )
    }
}
