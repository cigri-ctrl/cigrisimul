use crate::clock::Clock;
use crate::event::{Action, Event, EventList};
use crate::job::*;
use std::cell::RefCell;
use std::collections::{HashSet, VecDeque};
use rand_distr::{Normal, Distribution};

#[derive(Clone)]
pub struct FileSystem<'a> {
    nb_threads: usize,
    running_jobs: HashSet<JobId>,
    waiting_jobs: VecDeque<JobId>,
    event_list: &'a RefCell<EventList>,
    global_clock: &'a RefCell<Clock>,
    job_db: &'a RefCell<JobBank>,

    time_to_switch: f64,
    // ----- Loadavg stuff -----
    loadavg: f64,
    mean_value: f64,
    nb_iter: u8,
    noise_generator: Normal<f64>,
    noise: bool,
}

impl<'a> FileSystem<'a> {
    pub fn new(
        nb_threads: usize,
        global_clock: &'a RefCell<Clock>,
        event_list: &'a RefCell<EventList>,
        job_db: &'a RefCell<JobBank>,
        noise: bool,
    ) -> Self {
        FileSystem {
            nb_threads,
            running_jobs: HashSet::new(),
            waiting_jobs: VecDeque::new(),
            event_list: event_list,
            global_clock: global_clock,
            job_db,
            // time_to_switch: 0.1, // TODO: measure this value with expes
            time_to_switch: 0.0, // TODO: measure this value with expes
            // ----- Loadavg -----
            loadavg: 0.0,
            mean_value: 0.0,
            nb_iter: 0,
            // noise_generator: Normal::new(-1.992962e-18, 0.5880786*0.5880786).unwrap(),
            noise_generator: Normal::new(-1.992962e-18, 0.5880786).unwrap(),
            noise,
        }
    }

    fn get_write_time(&self, file_size: f64) -> f64 {
        // Call:
        // lm(formula = upper_bound_duration ~ filesize, data = overheads %>%
        //     filter(filesize %in% c(25, 50, 75) & sub_size == 10))

        // Residuals:
        //      Min       1Q   Median       3Q      Max
        // -0.10369 -0.10369  0.05184  0.05184  0.05184

        // Coefficients:
        //              Estimate Std. Error t value Pr(>|t|)
        // (Intercept) 3.626e-01  3.234e-03   112.1   <2e-16 ***
        // filesize    7.204e-02  5.988e-05  1203.1   <2e-16 ***
        // ---
        // Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

        // Residual standard error: 0.07334 on 3598 degrees of freedom
        // Multiple R-squared:  0.9975,	Adjusted R-squared:  0.9975
        // F-statistic: 1.447e+06 on 1 and 3598 DF,  p-value: < 2.2e-16

        let intercept = 0.3626;
        let filesize_coef = 0.07204;
        intercept + file_size * filesize_coef
    }

    pub fn update_loadavg(&mut self) {
        let fshift = 11;
        let fixed_1 = (1 << fshift) as f64;
        let exp_1 = 1884.0;

        let new_load = self.loadavg * exp_1 + self.mean_value * (fixed_1 - exp_1);

        self.loadavg = new_load / fixed_1;
    }

    pub fn get_loadavg(&self) -> f64 {
        if self.noise {
            let noise = self.noise_generator.sample(&mut rand::thread_rng());
            let output_sensor = self.loadavg + noise;
            if output_sensor < 0.0 {
                0.0
            } else {
                output_sensor
            }
        } else {
            self.loadavg
        }
    }

    pub fn get_nb_running(&self) -> usize {
        self.running_jobs.len()
    }

    pub fn get_nb_waiting_jobs(&self) -> usize {
        self.waiting_jobs.len()
    }

    pub fn get_nb_free_resources(&self) -> usize {
        self.nb_threads - self.running_jobs.len()
    }

    pub fn measure_nb_processes_running(&mut self) {
        self.mean_value = self.mean_value + self.get_nb_running() as f64;
        self.nb_iter = self.nb_iter + 1;

        if self.nb_iter == 4 {
            self.mean_value = self.mean_value / 5.0;
            self.update_loadavg();
            self.mean_value = 0.0;
            self.nb_iter = 0;
        }
        let new_event = Event::new(
            self.global_clock.borrow().get_time() + 1.0,
            Action::FileSystemMeasure,
        );
        self.event_list.borrow_mut().push(new_event);
    }

    pub fn submit_job_to_filesystem(&mut self, job_id: JobId) {
        if self.job_db.borrow().get_status(job_id) == JobStatus::Executing {
            self.waiting_jobs.push_back(job_id);
            let new_event = Event::new(0.0, Action::FileSystemSchedule);
            self.job_db
                .borrow_mut()
                .set_status(job_id, JobStatus::FileSystemWaiting);
            self.event_list.borrow_mut().push(new_event);
        }
    }

    pub fn schedule_jobs(&mut self) {
        let nb_free_resources = self.get_nb_free_resources();
        let mut i = 0;
        let initial_nb_of_waiting_jobs = self.get_nb_waiting_jobs();
        while i < initial_nb_of_waiting_jobs && i < nb_free_resources {
            let job_id = self.waiting_jobs.pop_front().unwrap();
            assert!(
                self.job_db.borrow().get_status(job_id) == JobStatus::FileSystemWaiting,
                "job #{} status found {:?}",
                job_id,
                self.job_db.borrow().get_status(job_id),
            );
            self.job_db
                .borrow_mut()
                .set_status(job_id, JobStatus::FileSystemExecuting);
            let new_event = Event::new(
                self.global_clock.borrow().get_time()
                    + self.time_to_switch
                    + self.get_write_time(self.job_db.borrow().get_file_size(job_id)),
                Action::FileSystemTerminate(job_id),
            );

            self.running_jobs.insert(job_id);
            self.event_list.borrow_mut().push(new_event);
            i = i + 1;
        }
    }

    pub fn terminate_job(&mut self, job_id: usize) {
        if self.job_db.borrow().get_status(job_id) == JobStatus::FileSystemExecuting {
            self.job_db
                .borrow_mut()
                .set_status(job_id, JobStatus::FileSystemDone);
            self.running_jobs.remove(&job_id);

            let event_schedule = Event::new(0.0, Action::FileSystemSchedule);
            self.event_list.borrow_mut().push(event_schedule);

            let event_oar = Event::new(0.0, Action::OARTerminate(job_id));
            self.event_list.borrow_mut().push(event_oar);
        }
    }

    pub fn kill_job(&mut self, job_id: JobId) {
        let _ = self.running_jobs.remove(&job_id);
        // TODO: berk
        self.waiting_jobs = self
            .waiting_jobs
            .iter()
            .filter(|&j_id| *j_id != job_id)
            .map(|x| *x)
            .collect();
        let event_schedule = Event::new(0.0, Action::FileSystemSchedule);
        self.event_list.borrow_mut().push(event_schedule);
    }
}
