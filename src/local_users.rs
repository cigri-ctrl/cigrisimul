use crate::event::*;
use crate::job::{JobBank, Priority};
use serde::Deserialize;
use std::cell::RefCell;

#[derive(Deserialize)]
pub struct LocalUsers {
    start_time: f64,
    job_duration: f64,
    file_size: f64,
    batch_size: usize,
    wait_between_batches: f64,
    iterations: usize,
}

impl LocalUsers {
    pub fn generate(&self, job_db: &RefCell<JobBank>, event_list: &RefCell<EventList>, notify_start: Option<f64>, notify_end: Option<f64>) {
        let mut sub_time = self.start_time;
        for _ in 0..self.iterations {
            for _ in 0..self.batch_size {
                let j_id = job_db.borrow_mut().create_new_job(
                    self.job_duration,
                    self.file_size,
                    Priority::Normal,
                );
                if let Some(notify_start_delay) = notify_start {
                    // Notifying of the start of a job
                    event_list
                        .borrow_mut()
                        .push(Event::new(sub_time - notify_start_delay, Action::OARNotifyJobStart));
                    // event_list
                    //     .borrow_mut()
                    //     .push(Event::new(sub_time, Action::OARUnNotifyJobStart));
                }

                if let Some(notify_end_delay) = notify_end {
                    // Notifying of the terminaison of a job
                    event_list
                        .borrow_mut()
                        .push(Event::new(sub_time + self.job_duration - notify_end_delay, Action::OARNotifyJobEnd));
                    // event_list
                    //     .borrow_mut()
                    //     .push(Event::new(sub_time + self.job_duration, Action::OARUnNotifyJobEnd));
                }
                event_list
                    .borrow_mut()
                    .push(Event::new(sub_time, Action::OARSubmit(j_id)));
            }
            sub_time = sub_time + self.wait_between_batches;
        }
    }
}
