use crate::cigri::CiGri;
use crate::clock::Clock;

use crate::file_system::FileSystem;
use crate::job::*;
use crate::oar::OAR;
use std::cell::RefCell;
use std::cmp::Ordering;
use std::collections::BinaryHeap;

#[derive(Clone, Debug)]
pub enum Action {
    OARSchedule,
    OARTerminate(JobId),
    OARSubmit(JobId),
    OARNotifyJobStart,
    OARUnNotifyJobStart,
    OARNotifyJobEnd,
    OARUnNotifyJobEnd,
    FileSystemMeasure,
    FileSystemSubmit(JobId),
    FileSystemSchedule,
    FileSystemTerminate(JobId),
    CiGriMainLoop,
    Finish,
}

#[derive(Clone, Debug)]
pub struct Event {
    timestamp: f64,
    action: Action,
}

impl Event {
    pub fn new(timestamp: f64, action: Action) -> Self {
        Event { timestamp, action }
    }

    pub fn get_timestamp(&self) -> f64 {
        self.timestamp
    }

    // pub fn exec_action(&self) {
    //     let id = self.job_id.unwrap_or(0);
    //     (self.action)(id);
    // }
}

// WARNING: Changed the order of the cmp to have the smaller timestamp on top of the heap without
// using cmp::Reverse
impl Ord for Event {
    fn cmp(&self, other: &Event) -> Ordering {
        // self.timestamp.partial_cmp(&other.timestamp).unwrap()
        other.timestamp.partial_cmp(&self.timestamp).unwrap()
    }
}

impl PartialEq for Event {
    fn eq(&self, other: &Self) -> bool {
        self.timestamp == other.timestamp
    }
}

impl Eq for Event {}

impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Clone, Debug)]
pub struct EventList {
    list: BinaryHeap<Event>,
}

impl EventList {
    pub fn new() -> Self {
        EventList {
            list: BinaryHeap::new(),
        }
    }

    pub fn push(&mut self, event: Event) {
        self.list.push(event);
    }

    pub fn pop(&mut self) -> Option<Event> {
        self.list.pop()
    }

    pub fn clear(&mut self) {
        self.list.clear()
    }
}

#[derive(Clone)]
pub struct EventManager<'a> {
    pub event_list: &'a RefCell<EventList>,
    oar: &'a RefCell<OAR<'a>>,
    cigri: &'a RefCell<CiGri<'a>>,
    clock: &'a RefCell<Clock>,
    fs: &'a RefCell<FileSystem<'a>>,
}

impl<'a> EventManager<'a> {
    pub fn new(
        clock: &'a RefCell<Clock>,
        event_list: &'a RefCell<EventList>,
        oar: &'a RefCell<OAR<'a>>,
        cigri: &'a RefCell<CiGri<'a>>,
        fs: &'a RefCell<FileSystem<'a>>,
    ) -> Self {
        EventManager {
            clock,
            event_list: event_list,
            oar,
            cigri,
            fs,
        }
    }

    pub fn exec_action(&mut self, e: Event) {
        match e.action {
            Action::OARSchedule => self.oar.borrow_mut().schedule_jobs(),
            Action::OARTerminate(id) => self.oar.borrow_mut().terminate_job(id),
            Action::OARSubmit(id) => self.oar.borrow_mut().submit_job(id),
            Action::OARNotifyJobStart | Action::OARUnNotifyJobEnd => self.oar.borrow_mut().notify_start(),
            Action::OARNotifyJobEnd | Action::OARUnNotifyJobStart => self.oar.borrow_mut().notify_end(),
            Action::CiGriMainLoop => self.cigri.borrow_mut().run(),
            Action::Finish => self.event_list.borrow_mut().clear(),
            Action::FileSystemSubmit(id) => self.fs.borrow_mut().submit_job_to_filesystem(id),
            Action::FileSystemMeasure => self.fs.borrow_mut().measure_nb_processes_running(),
            Action::FileSystemSchedule => self.fs.borrow_mut().schedule_jobs(),
            Action::FileSystemTerminate(id) => self.fs.borrow_mut().terminate_job(id),
        }
    }

    pub fn pop(&mut self) -> Option<Event> {
        self.event_list.borrow_mut().pop()
    }

    pub fn run(&mut self) {
        let mut event = self.pop();
        let mut max_time = self.clock.borrow().get_time();
        while let Some(e) = event {
            // println!("Event: {:?}", e);
            let ts = e.get_timestamp();
            if ts > max_time {
                max_time = ts;
                self.clock.borrow_mut().set_time(max_time);
            }
            self.exec_action(e);
            event = self.pop();
        }
    }
}
