use sigrilib::start_simul_from_file;
use clap::{App, Arg};

fn main() {
    let matches = App::new("cigri-simul")
        .version("1.0")
        .author("Quentin Guilloteau")
        .about("Simulate CiGri-Ctrl Behaviours")
        .arg(
            Arg::with_name("INPUT")
                .required(true)
                .value_name("SIMUL_FILE.yml")
                .help("Path to the simulation file"),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .required(false)
                .value_name("data.csv")
                .help("Output file"),
        )
        .get_matches();

    let filename = matches.value_of("INPUT").unwrap_or("expe.yml");
    let outname = matches.value_of("OUTPUT").unwrap_or("data.csv");
    start_simul_from_file(filename, outname)
}
