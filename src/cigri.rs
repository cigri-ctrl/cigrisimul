use crate::clock::Clock;
use crate::controllers::*;
use crate::event::{Action, Event, EventList};
use crate::job::*;
use crate::oar::OAR;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet, VecDeque};
use std::env;
use std::fs::File;
use std::io::Write;


#[derive(Clone)]
pub struct CiGri<'a> {
    event_list: &'a RefCell<EventList>,
    global_clock: &'a RefCell<Clock>,
    job_db: &'a RefCell<JobBank>,
    oar: &'a RefCell<OAR<'a>>,
    sleep_time: f64,
    campaigns: HashMap<usize, Campaign>,
    previous_submission_ids: HashSet<usize>,
    controller: &'a RefCell<&'a mut Box<ControllerType>>,
    jobs_to_submit: HashMap<IOWeight, VecDeque<JobId>>,
    outfile: String,
}

impl<'a> CiGri<'a> {
    pub fn new(
        sleep_time: f64,
        global_clock: &'a RefCell<Clock>,
        event_list: &'a RefCell<EventList>,
        job_db: &'a RefCell<JobBank>,
        oar: &'a RefCell<OAR<'a>>,
        controller: &'a RefCell<&'a mut Box<ControllerType>>,
    ) -> Self {
        let outfile = format!(
            "time, sub_size, percentage, waiting, running, load, cumulated_be_killed{}",
            controller.borrow().controller_data_header(),
        );
        CiGri {
            sleep_time,
            event_list,
            global_clock,
            job_db,
            campaigns: HashMap::new(),
            previous_submission_ids: HashSet::new(),
            oar,
            controller,
            jobs_to_submit: HashMap::new(),
            outfile,
        }
    }

    pub fn get_log(&self) -> String {
        self.outfile.clone()
    }

    pub fn submit(&mut self, c: Campaign) {
        c.jobs.iter().for_each(|&id| {
            self.jobs_to_submit
                .entry(c.get_io_weight())
                .or_insert(VecDeque::new())
                .push_front(id);
            // self.jobs_to_submit
            //     .get_mut(&c.get_io_weight())
            //     .unwrap()
            //     .push_front(id);
        });
        let c_id = c.get_id();
        self.campaigns.insert(c_id, c);
    }

    pub fn submit_jobs_to_oar(&self, jobs_to_submit: VecDeque<JobId>) {
        jobs_to_submit.iter().for_each(|&j| {
            self.event_list
                .borrow_mut()
                .push(Event::new(0.0, Action::OARSubmit(j)));
            self.job_db
                .borrow_mut()
                .set_status(j, JobStatus::OARSubmitted);
        });
    }

    fn get_jobs_to_send_to_oar_helper(
        &mut self,
        nb_jobs: usize,
        io_weight: IOWeight,
    ) -> VecDeque<JobId> {
        let len = self
            .jobs_to_submit
            .get(&io_weight)
            .map(|o| o.len())
            .unwrap_or(0);
        let sub_size = if nb_jobs > len { 0 } else { len - nb_jobs };
        self.jobs_to_submit
            .entry(io_weight)
            .or_insert(VecDeque::new())
            // .get_mut(&io_weight)
            // .unwrap()
            .split_off(sub_size)
    }

    pub fn get_jobs_to_send_to_oar(&mut self, nb_jobs: usize, percentage: f64) -> VecDeque<JobId> {
        let (nb_jobs_heavy, nb_jobs_light) = if self
            .jobs_to_submit
            .entry(IOWeight::IOHeavy)
            .or_insert(VecDeque::new())
            .len()
            == 0
        {
            let nb_jobs_light = ((nb_jobs as f64) * percentage) as usize;
            let nb_jobs_heavy = nb_jobs - nb_jobs_light;
            (nb_jobs_heavy, nb_jobs_light)
        } else {
            let nb_jobs_heavy = ((nb_jobs as f64) * percentage) as usize;
            let nb_jobs_light = nb_jobs - nb_jobs_heavy;
            (nb_jobs_heavy, nb_jobs_light)
        };

        let mut jobs_heavy = self.get_jobs_to_send_to_oar_helper(nb_jobs_heavy, IOWeight::IOHeavy);
        let mut jobs_light = self.get_jobs_to_send_to_oar_helper(nb_jobs_light, IOWeight::IOLight);

        jobs_heavy.append(&mut jobs_light);
        jobs_heavy
    }

    pub fn is_time_for_a_new_submission(&self) -> bool {
        if !self
            .controller
            .borrow()
            .wait_for_previous_submission_to_finish()
        {
            true
        } else {
            !self
                .previous_submission_ids
                .iter()
                .any(|id| !self.job_db.borrow().has_been_executed(*id))
        }
    }

    pub fn run(&mut self) {
        if self
            .jobs_to_submit
            .iter()
            .map(|(_k, v)| v.len())
            .sum::<usize>()
            == 0
        {
            self.event_list
                .borrow_mut()
                .push(Event::new(0.0, Action::Finish));
            return;
        }
        let sensors_values = self.oar.borrow().get_sensor_values();
        let mut n = 0;
        let mut percentage = 1.0;
        if self.is_time_for_a_new_submission() {
            self.controller
                .borrow_mut()
                .update_submission_size(&sensors_values);
            n = self.controller.borrow().get_submission_size() as usize;
            percentage = self.controller.borrow().get_percentage();
            let jobs: VecDeque<JobId> = self.get_jobs_to_send_to_oar(n, percentage);
            self.submit_jobs_to_oar(jobs);
        }
        self.outfile = 
        format!(
            "{}\n{}, {:.2}, {}, {}, {}, {}, {:.2}{}",
            self.outfile,
            self.global_clock.borrow().get_time(),
            n,
            percentage,
            sensors_values.oar_waiting,
            sensors_values.oar_running,
            sensors_values.loadavg,
            sensors_values.total_be_jobs_killed,
            self.controller.borrow().controller_data(),
        );
        self.event_list.borrow_mut().push(Event::new(
            self.global_clock.borrow().get_time() + self.sleep_time,
            Action::CiGriMainLoop,
        ));
    }
}
