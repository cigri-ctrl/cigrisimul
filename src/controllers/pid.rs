use crate::controllers::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize, Default)]
pub struct PIDController {
    #[serde(default)]
    sub_size: f64,
    #[serde(default)]
    kp: f64,
    #[serde(default)]
    ki: f64,
    #[serde(default)]
    kd: f64,
    sensor: Sensor,
    #[serde(skip)]
    cumulated_error: f64,
    #[serde(skip)]
    previous_error: f64,
    reference: f64,
}

// #[typetag::serde]
impl Controller for PIDController {
    fn get_submission_size(&self) -> f64 {
        self.sub_size
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues) {
        let error = self.reference - sensors_values.get_value(&self.sensor);
        self.cumulated_error = self.cumulated_error + error;
        self.sub_size = self.kp * error
            + self.ki * self.cumulated_error
            + self.kd * (error - self.previous_error);
        self.previous_error = error;
    }
}
