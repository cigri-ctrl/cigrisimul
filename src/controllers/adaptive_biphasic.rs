use crate::controllers::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct AdaptiveBiphasic {
    #[serde(default)]
    sub_size: f64,
    #[serde(default)]
    percentage: f64,
    #[serde(default = "default_threshold")]
    threshold: f64,
    min_threshold: f64,
    // Load
    #[serde(default = "default_kp_jobs")]
    kp_load_jobs: f64,
    #[serde(default = "default_kp_percentage")]
    kp_load_percentage: f64,

    ref_load: f64,
    // WQ
    kp_wq: f64,
    ref_wq: f64,
}

fn default_kp_jobs() -> f64 {
    0.5
}

fn default_kp_percentage() -> f64 {
    0.1
}

fn default_threshold() -> f64 {
    1.5
}

fn bound<T: PartialOrd>(x: T, min: T, max: T) -> T {
    if x > max {
        max
    } else if x < min {
        min
    } else {
        x
    }
}

// #[typetag::serde]
impl Controller for AdaptiveBiphasic {
    fn get_submission_size(&self) -> f64 {
        self.sub_size
    }

    fn get_percentage(&self) -> f64 {
        self.percentage
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues) {
        if self.sub_size > 0.0 && self.threshold > self.min_threshold {
            self.kp_load_percentage = 1.0 / (self.sub_size * self.threshold);
            self.kp_load_jobs = 1.0 / self.threshold;
        }

        // Errors
        let error_wq = self.ref_wq - (sensors_values.oar_waiting as f64);
        let error_load = self.ref_load - sensors_values.loadavg;
        if error_load.abs() >= self.min_threshold {
            if error_load.abs() < self.threshold {
                let new_percentage = self.percentage + self.kp_load_percentage * error_load;
                if (self.percentage == 1.0 && new_percentage > self.percentage)
                    || (self.percentage == 0.0 && new_percentage < self.percentage)
                {
                    self.threshold = self.threshold / 1.5;
                    self.percentage = 0.5;
                } else {
                    self.percentage = bound(new_percentage, 0.0, 1.0);
                }
            } else {
                let sub_wq = bound(self.sub_size + self.kp_wq * error_wq, 0.0, f64::MAX);
                let sub_load = bound(
                    self.sub_size + self.kp_load_jobs * error_load,
                    0.0,
                    f64::MAX,
                );
                self.sub_size = if sub_wq < sub_load { sub_wq } else { sub_load };
                // ---- Adaptive gain for percentage
            }
        }
    }

    fn wait_for_previous_submission_to_finish(&self) -> bool {
        false
    }

    fn controller_data(&self) -> String {
        format!(", {}", self.threshold)
    }

    fn controller_data_header(&self) -> String {
        String::from(", threshold")
    }
}
