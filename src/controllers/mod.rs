// pub mod adaptive_biphasic;
pub mod biphasic;
pub mod composition;
pub mod intelligent_pid;
pub mod naive;
pub mod pid;
// pub use adaptive_biphasic::AdaptiveBiphasic;
pub use biphasic::Biphasic;
pub use composition::Composition;
pub use intelligent_pid::MFC;
pub use pid::PIDController;

use serde::{Deserialize, Serialize};
use std::ops::Add;

#[derive(Clone, Serialize, Deserialize)]
// #[serde(tag = "type")]
pub enum Sensor {
    OarWaiting,
    OarRunning,
    Loadavg,
    OarBestEffortRunning,
    OarBestEffortWaiting,
    OarBestEffortRunningPlusOarBestEffortWaiting,
}

impl Default for Sensor {
    fn default() -> Self {
        Sensor::Loadavg
    }
}

#[derive(Clone, Deserialize, Serialize, Copy)]
pub enum SensorCompositionFunction {
    Sum,
    Min,
    Max,
}

impl Default for SensorCompositionFunction {
    fn default() -> Self {
        SensorCompositionFunction::Min
    }
}

#[derive(Deserialize, Serialize, Default, Clone)]
pub struct SensorComposition {
    sensors: Vec<Sensor>,
    composition_f: SensorCompositionFunction,
}

impl SensorComposition {
    fn compute_sensor_value(&self, values: &SensorValues) -> f64 {
        self.sensors
            .iter()
            .map(|c| values.get_value(c))
            .reduce(match self.composition_f {
                SensorCompositionFunction::Min => f64::min,
                SensorCompositionFunction::Max => f64::max,
                SensorCompositionFunction::Sum => f64::add,
            })
            .unwrap_or(0.0)
    }
}

#[derive(Clone, Copy)]
pub struct SensorValues {
    pub oar_waiting: usize,
    pub oar_running: usize,
    pub loadavg: f64,
    pub oar_best_effort_running: usize,
    pub oar_best_effort_waiting: usize,
    pub notify_jobs: isize,
    pub total_be_jobs_killed: f64,
}

impl SensorValues {
    pub fn new(oar_waiting: usize, oar_running: usize, loadavg: f64, oar_best_effort_running: usize, oar_best_effort_waiting: usize, notify_jobs: isize, total_be_jobs_killed: f64) -> Self {
        SensorValues {
            oar_waiting,
            oar_running,
            loadavg,
            oar_best_effort_running,
            oar_best_effort_waiting,
            notify_jobs,
            total_be_jobs_killed,
        }
    }

    pub fn get_value(&self, sensor: &Sensor) -> f64 {
        match sensor {
            Sensor::OarWaiting => self.oar_waiting as f64,
            Sensor::OarRunning => self.oar_running as f64,
            // Sensor::OarRunningPlusOarWaiting => (self.oar_running as isize + self.oar_waiting as isize + self.notify_jobs) as f64,
            Sensor::OarBestEffortRunningPlusOarBestEffortWaiting => (self.oar_best_effort_running as isize + self.oar_best_effort_waiting as isize + self.notify_jobs) as f64,
            Sensor::Loadavg => self.loadavg,
            Sensor::OarBestEffortRunning => self.oar_best_effort_running as f64,
            Sensor::OarBestEffortWaiting => self.oar_best_effort_waiting as f64,
            // Sensor::SensorComposition(compo) => compo.compute_sensor_value(&self),
        }
    }
}

macro_rules! with_dollar_sign {
    ($($body:tt)*) => {
        macro_rules! __with_dollar_sign { $($body)* }
        __with_dollar_sign!($);
    }
}



macro_rules! generate_enum_and_apply_macro {
    ($name:ident, $($type:ident),*) => {
        #[derive(Deserialize, Serialize)]
        pub enum $name {
            $(
                $type($type),
            )*
        }
        with_dollar_sign! {
            ($d:tt) => {
                macro_rules! apply_on_match {
                    ($d self:ident, $d function_name:ident, $d ($d arg:expr),*) => {
                        match &$d self {
                            $(
                                $name::$type(x) => x.$d function_name($d ($d arg),*),
                            )*
                        }
                    }
                }

                macro_rules! apply_on_match_mut {
                    (mut $d self:ident, $d function_name:ident, $d ($d arg:expr),*) => {
                        match $d self {
                            $(
                                $name::$type(ref mut x) => x.$d function_name($d ($d arg),*),
                            )*
                        }
                    }
                }
            }
        }
    }
}

generate_enum_and_apply_macro!(ControllerType, PIDController, MFC, Biphasic, Composition);


impl Controller for ControllerType {
    fn get_submission_size(&self) -> f64 {
        apply_on_match!(self, get_submission_size,)
    }

    fn get_percentage(&self) -> f64 {
        apply_on_match!(self, get_percentage,)
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues) {
        apply_on_match_mut!(mut self, update_submission_size, sensors_values)
    }

    fn wait_for_previous_submission_to_finish(&self) -> bool {
        apply_on_match!(self, wait_for_previous_submission_to_finish,)
    }

    fn controller_data(&self) -> String {
        apply_on_match!(self, controller_data,)
    }

    fn controller_data_header(&self) -> String {
        apply_on_match!(self, controller_data_header,)
    }

}

// #[typetag::serde(tag = "type")]
pub trait Controller {
    fn get_submission_size(&self) -> f64;

    fn get_percentage(&self) -> f64 {
        1.0
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues);

    fn wait_for_previous_submission_to_finish(&self) -> bool {
        false
    }

    fn controller_data(&self) -> String {
        String::new()
    }

    fn controller_data_header(&self) -> String {
        String::new()
    }
}
