use crate::controllers::*;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct MFC {
    // controller: Box<dyn Controller>,
    controller: Box<ControllerType>,
    alpha: f64,
    h: f64,
    sensor: Sensor,
    #[serde(default)]
    previous_y: f64,
    #[serde(default)]
    estimation_model: f64,
    #[serde(default)]
    sub_size: f64,
}

// #[typetag::serde]
impl Controller for MFC {
    fn get_submission_size(&self) -> f64 {
        self.sub_size 
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues) {
        let y = sensors_values.get_value(&self.sensor);
        self.estimation_model = (y - self.previous_y) / self.h - self.alpha * self.sub_size;
        self.controller.update_submission_size(sensors_values);
        self.sub_size =
            -(self.estimation_model - self.controller.get_submission_size()) / self.alpha;
        self.previous_y = y;
    }

    fn controller_data(&self) -> String {
        format!(", {}", self.estimation_model / self.alpha)
    }

    fn controller_data_header(&self) -> String {
        String::from(", F")
    }
}
