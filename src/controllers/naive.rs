use crate::controllers::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct NaiveController {
    sub_size: f64,
}

// #[typetag::serde]
impl Controller for NaiveController {
    fn get_submission_size(&self) -> f64 {
        self.sub_size
    }

    fn update_submission_size(&mut self, _sensors_values: &SensorValues) {}

    fn wait_for_previous_submission_to_finish(&self) -> bool {
        true
    }
}
