use crate::controllers::*;
use serde::{Deserialize, Serialize};
use std::ops::Add;

#[derive(Clone, Deserialize, Serialize)]
pub enum CompositionFunction {
    Sum,
    Min,
    Max,
}

impl Default for CompositionFunction {
    fn default() -> Self {
        CompositionFunction::Min
    }
}

#[derive(Deserialize, Serialize, Default)]
pub struct Composition {
    controllers: Vec<Box<ControllerType>>,
    composition_f: CompositionFunction,
}

// #[typetag::serde]
impl Controller for Composition {
    fn get_submission_size(&self) -> f64 {
        self.controllers
            .iter()
            .map(|c| c.get_submission_size())
            .reduce(match self.composition_f {
                CompositionFunction::Min => f64::min,
                CompositionFunction::Max => f64::max,
                CompositionFunction::Sum => f64::add,
            })
            .unwrap_or(0.0)
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues) {
        self.controllers
            .iter_mut()
            .for_each(|c| c.update_submission_size(sensors_values));
    }

    fn controller_data(&self) -> String {
        self.controllers.iter().fold(String::new(), |acc, x| {
            format!("{}, {}", acc, x.controller_data())
        })
    }

    fn controller_data_header(&self) -> String {
        self.controllers.iter().fold(String::new(), |acc, x| {
            format!("{}, {}", acc, x.controller_data_header())
        })
    }
}
