use crate::controllers::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Biphasic {
    #[serde(default)]
    sub_size: f64,
    #[serde(default)]
    percentage: f64,

    reference: f64,
    threshold: f64,

    // controller_nb_jobs: Box<dyn Controller>,
    // controller_percentage: Box<dyn Controller>,
    controller_nb_jobs: Box<ControllerType>,
    controller_percentage: Box<ControllerType>,
    sensor: Sensor,
}

fn bound<T: PartialOrd>(x: T, min: T, max: T) -> T {
    if x > max {
        max
    } else if x < min {
        min
    } else {
        x
    }
}

// #[typetag::serde]
impl Controller for Biphasic {
    fn get_submission_size(&self) -> f64 {
        self.sub_size
    }

    fn get_percentage(&self) -> f64 {
        bound(0.5 + self.percentage, 0.0, 1.0)
    }

    fn update_submission_size(&mut self, sensors_values: &SensorValues) {
        // Errors
        let error = self.reference - sensors_values.get_value(&self.sensor);
        if error.abs() < self.threshold {
            self.controller_percentage
                .update_submission_size(sensors_values);
            self.percentage = self.controller_percentage.get_submission_size() as f64 / 100.0;
        } else {
            self.controller_nb_jobs
                .update_submission_size(sensors_values);
            self.sub_size = self.controller_nb_jobs.get_submission_size() as f64;
        }
    }
}
