{
  description = "Flake Presentation";

  outputs = { self, nixpkgs }: {

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.slides;

    packages.x86_64-linux.slides =
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "slides";
        buildInputs = [
          pandoc
          R
          rPackages.rmarkdown
          rPackages.ggplot2
          rPackages.gridExtra
          rPackages.markdown
          rPackages.knitr
          texlive.combined.scheme-full
        ];
        src = ./.;
        installPhase = ''
          Rscript -e 'rmarkdown::render("main.Rmd", "beamer_presentation")'
          mv main.pdf $out
        '';
      };

  };
}
