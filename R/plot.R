#! /usr/bin/env nix-shell
#! nix-shell -i Rscript -p R rPackages.tidyverse rPackages.gridExtra rPackages.reshape2

args <- commandArgs()

filename <- args[length(args)]

library(tidyverse)
library(gridExtra)
library(reshape2)

df <- read_csv(filename)
# colnames(df) <- c("time", "sub_size", "percentage", "waiting", "running", "load", "t")

compute_cluster_usage <- function(df, nb_nodes = 100) {
    # dt <- mean(diff(df$time))
    dt <- 30
    sum(df$running * dt / nb_nodes) / max(df$time)
}

compute_load_mean_error <- function(load, ref = 4) {
    mean(abs(load - ref), na.rm = TRUE)
}

df %>%
    select(time, sub_size, waiting, running, load) %>%
    melt(id.vars = c("time")) %>%
    ggplot(aes(x = time, y = value)) +
    geom_step() +
    facet_grid(variable ~ ., scales = "free") +
    ylim(0, NA) +
    ylab("Values") +
    xlab("Time") +
    ggtitle("Summary") +
    theme_bw()

df_area <- df %>%
    mutate(
        cumulated_lost_time = cumsum(100 - running) * 30,
    ) %>%
    select(time, cumulated_lost_time, cumulated_be_killed)

max_time <- max(df_area$time)
total_time_lost <- max(df_area$cumulated_lost_time) + max(df_area$cumulated_be_killed)
percentage_lost <- total_time_lost / (max_time * 100)

df_area %>%
    melt(id.vars = c("time")) %>%
    ggplot(aes(x = time, y = value, fill = variable)) +
    geom_area() +
    ylim(0, NA) +
    ylab("CPU seconds lost") +
    xlab("Time") +
    ggtitle(paste("Total lost time:", total_time_lost, "secs (", sprintf("%.3f", 100 * percentage_lost), "%)")) +
    theme_bw() +
    theme(legend.position = "bottom")



# plot_running <- ggplot(df) +
#     geom_step(aes(x = time, y = running)) +
#     ylab("Running") +
#     xlab("Time") +
#     ggtitle("Running") +
#     ggtitle(paste("Running", ": ", compute_cluster_usage(df) * 100 , "%", sep = " ")) +
#     theme_bw()
# 
# plot_waiting <- ggplot(df) +
#     geom_step(aes(x = time, y = waiting)) +
#     ylab("Waiting") +
#     xlab("Time") +
#     ggtitle("Waiting") +
#     theme_bw()
# 
# plot_load <- ggplot(df) +
#     geom_step(aes(x = time, y = load)) +
#     ylab("Load") +
#     xlab("Time") +
#     ggtitle("Load") +
#     ggtitle(paste("Load", ": ", compute_load_mean_error(df$load), sep = " ")) +
#     theme_bw()
# 
# plot_sub_size <- ggplot(df) +
#     geom_step(aes(x = time, y = sub_size)) +
#     ylab("Sub. Size") +
#     xlab("Time") +
#     ggtitle("Sub. Size") +
#     theme_bw()
# 
# plot_percentage <- ggplot(df) +
#     geom_step(aes(x = time, y = percentage * 100)) +
#     ylab("Percentage IO heavy") +
#     xlab("Time") +
#     ggtitle("Percentage") +
#     ylim(0, 100) +
#     theme_bw()
# 
# ma <- function(x, n = 5){filter(x, rep(1 / n, n), sides = 2)}
# 
# plot_load_ma <- ggplot(df) +
#     geom_step(aes(x = time, y = ma(load))) +
#     ylab("Load") +
#     xlab("Time") +
#     ggtitle("Moving Avg. Load") +
#     ggtitle(paste("Moving Avg. Load", ": ", compute_load_mean_error(ma(df$load)), sep = " ")) +
#     theme_bw()
# 
# grid.arrange(plot_running, plot_waiting, plot_load, plot_sub_size, ncol = 2)
# 
# grid.arrange(plot_running, plot_sub_size, plot_load_ma, plot_percentage, ncol = 2)


