{
  description = "sigri";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    utils.url = "github:numtide/flake-utils";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, utils, rust-overlay }:
    let
      system = "x86_64-linux";
      # src = ./.;
      src = pkgs.lib.sourceByRegex ./. [
        "^Cargo\.toml$"
        "^Cargo\.lock$"
        "^src$"
        "^src/.\+\.rs$"
        "^src/controllers$"
        "^src/controllers/.\+\.rs$"
      ];
      srcR = pkgs.lib.sourceByRegex ./. [
        "^R$"
        "^R/plot\.R$"
      ];
      pname = "sigri";
      overlays = [ rust-overlay.overlay ];
      pkgs = import nixpkgs { inherit overlays system; };

      rPkgs = with pkgs.rPackages; [ tidyverse reshape2 gridExtra ];

      myR = pkgs.rWrapper.override { packages = rPkgs; };

    in {
      packages.${system} = rec {
        default = self.packages.${system}.${pname};
        ${pname} = pkgs.rustPlatform.buildRustPackage rec {
          inherit pname src;
          version = "0.0.1";
          cargoSha256 = "sha256-LjUaq9nxw3/R6ifM5mxMcLOGBOiAgbqkoD3u0DZtLx8=";
          verifyCargoDeps = true;
        };

        "${pname}-summary" = pkgs.writeScriptBin "${pname}-summary" ''
          ${myR}/bin/Rscript ${srcR}/R/plot.R $1
        '';

        "${pname}-docker" = pkgs.dockerTools.buildImage {
          name = "registry.gitlab.inria.fr/cigri-ctrl/cigrisimul";
          tag = "latest";

          contents = self.packages.${system}.${pname};
          config = {
            # docker run -it --rm -v "$(pwd):/data" sigri experiments/mfc.yml
            Entrypoint = [ "${self.packages.${system}.${pname}}/bin/sigri" ];
            WorkingDir = "/data";
            Volumes = { "/data" = { }; };
          };

        };

        "${pname}-wasm" = pkgs.rustPlatform.buildRustPackage {
          pname = "${pname}-wasm";
          version = "1.0.0";
          cargoSha256 = "sha256-oDZhmeFXsHF0JmxzrzAjvtTUEw4Bp1SAL9mhC8CbOac=";

          inherit src;
          nativeBuildInputs = [
            pkgs.wasm-bindgen-cli
            pkgs.cargo
            (pkgs.rust-bin.stable.latest.default.override {
              extensions = [ "rust-src" ];
              targets = [ "wasm32-unknown-unknown" ];
            })
          ];

          buildPhase = ''
            cargo build --release --target=wasm32-unknown-unknown
            mkdir -p $out;
            wasm-bindgen \
              --target web \
              --out-dir $out \
              target/wasm32-unknown-unknown/release/${pname}lib.wasm;
          '';
          installPhase = "echo 'Skipping installPhase'";
        };
      };
    };
}
